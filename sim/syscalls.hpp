//--------------------------------------------------------------------------------------------------
// Copyright (c) 2020 Marcus Geelnard
//
// This software is provided 'as-is', without any express or implied warranty. In no event will the
// authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose, including commercial
// applications, and to alter it and redistribute it freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented; you must not claim that you wrote
//     the original software. If you use this software in a product, an acknowledgment in the
//     product documentation would be appreciated but is not required.
//
//  2. Altered source versions must be plainly marked as such, and must not be misrepresented as
//     being the original software.
//
//  3. This notice may not be removed or altered from any source distribution.
//--------------------------------------------------------------------------------------------------

#ifndef SIM_SYSCALLS_HPP_
#define SIM_SYSCALLS_HPP_

#include "ram.hpp"

#include <array>
#include <cstdint>
#include <string>

#include <sys/stat.h>
#include <sys/types.h>

#if defined(_WIN32)
using stat_t = struct _stat64;
struct win_dir_t;
struct win_dirent_t;
using dir_handle_t = win_dir_t*;
using dirent_t = win_dirent_t*;
#else
using stat_t = struct stat;
using dir_handle_t = void*;
struct dirent;
using dirent_t = struct dirent*;
#endif

class syscalls_t {
public:
  // Simulator routines.
  enum class routine_t {
    EXIT = 0,
    PUTCHAR = 1,
    GETCHAR = 2,
    CLOSE = 3,
    FSTAT = 4,
    ISATTY = 5,
    LINK = 6,
    LSEEK = 7,
    MKDIR = 8,
    OPEN = 9,
    READ = 10,
    STAT = 11,
    UNLINK = 12,
    WRITE = 13,
    GETTIMEMICROS = 14,
    RMDIR = 15,
    GETARGUMENTS = 16,
    OPENDIR = 17,
    FDOPENDIR = 18,
    CLOSEDIR = 19,
    READDIR = 20,
    LAST_
  };

  syscalls_t(ram_t& ram);
  ~syscalls_t();

  /// @brief Use a disk image for file system operations.
  /// @param disk_image_path Path to the disk image.
  /// @returns true if the disk image was successfully mounted.
  bool mount_disk_image(const std::string& disk_image_path);

  /// @brief Unmount the disk image.
  void unmount_disk_image();

  /// @brief Clear the run state.
  void clear();

  /// @brief Call a system routine.
  /// @param routine_no Syscall routine ID.
  /// @param regs A mutable array of the current register state.
  void call(const uint32_t routine_no, std::array<uint32_t, 33>& regs);

  /// @returns true if a call requested the process to terminate.
  bool terminate() const {
    return m_terminate;
  }

  /// @returns the exit code for the process.
  uint32_t exit_code() const {
    return m_exit_code;
  }

private:
  void stat_to_ram(stat_t& buf, uint32_t addr);
  std::string path_to_host(uint32_t addr);
  int fd_to_host(uint32_t fd);
  uint32_t fd_to_guest(int fd);
  int open_flags_to_host(uint32_t flags);
  int open_mode_to_host(uint32_t flags);
  dir_handle_t dirp_to_host(uint32_t dirp);
  uint32_t dirp_to_guest(dir_handle_t dirp);
  void dirent_to_ram(dirent_t ent, uint32_t addr);

  void sim_exit(int status);
  int sim_putchar(int c);
  int sim_getchar(void);
  int sim_close(int fd);
  int sim_fstat(int fd, stat_t* buf);
  int sim_isatty(int fd);
  int sim_link(const char* oldpath, const char* newpath);
  int sim_lseek(int fd, int offset, int whence);
  int sim_mkdir(const char* pathname, int mode);
  int sim_open(const char* pathname, int flags, int mode);
  int sim_read(int fd, char* buf, int nbytes);
  int sim_stat(const char* path, stat_t* buf);
  int sim_unlink(const char* pathname);
  int sim_write(int fd, const char* buf, int nbytes);
  unsigned long long sim_gettimemicros(void);
  int sim_rmdir(const char* pathname);
  dir_handle_t sim_opendir(const char* dirname);
  dir_handle_t sim_fdopendir(int fd);
  int sim_closedir(dir_handle_t dirp);
  dirent_t sim_readdir(dir_handle_t dirp);

  ram_t& m_ram;

  int m_disk_fd = -1;

  bool m_terminate = false;
  uint32_t m_exit_code = 0u;

  static constexpr int MAX_OPEN_DIRS = 32;
  dir_handle_t m_open_dirs[MAX_OPEN_DIRS];

  static constexpr uint32_t DIRP_BASE_ADDR = 0xfffff000U;
  static constexpr uint32_t DIRENT_ADDR = DIRP_BASE_ADDR + (MAX_OPEN_DIRS * 4);
};

#endif  // SIM_SYSCALLS_HPP_
