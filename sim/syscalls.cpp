//--------------------------------------------------------------------------------------------------
// Copyright (c) 2020 Marcus Geelnard
//
// This software is provided 'as-is', without any express or implied warranty. In no event will the
// authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose, including commercial
// applications, and to alter it and redistribute it freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented; you must not claim that you wrote
//     the original software. If you use this software in a product, an acknowledgment in the
//     product documentation would be appreciated but is not required.
//
//  2. Altered source versions must be plainly marked as such, and must not be misrepresented as
//     being the original software.
//
//  3. This notice may not be removed or altered from any source distribution.
//--------------------------------------------------------------------------------------------------

#include "syscalls.hpp"

#include "third_party/mfat/mfat.h"

#include <stdio.h>
#include <cstring>
#include <stdexcept>

#if defined(_WIN32)
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <windows.h>
#undef ERROR
#undef log
#include <direct.h>
#include <io.h>
#else
#include <dirent.h>
#include <sys/time.h>
#include <unistd.h>
#endif

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>

#if defined(_WIN32)
// MSVC does not define _O_ACCMODE (but MinGW does).
#if !defined(_O_ACCMODE)
#define _O_ACCMODE (_O_RDONLY | _O_WRONLY | _O_RDWR)
#endif

struct win_dirent_t {
  WIN32_FIND_DATAA find_file_data;
};

struct win_dir_t {
  HANDLE handle;
  win_dirent_t dirent;
  bool first;
};
#endif

namespace {
constexpr uint32_t SIM_ARGS_START = 0xfff00000U;

void sleep_to_simulate_slow_block_access(void) {
  // This is roughly the time it takes to read a block (512 bytes) from an SD-card on an MC1.
  constexpr int BLOCK_IO_TIME_MICROS = 40;  // Time in microseconds.

#if defined(_WIN32)
  // TODO(m): Implement a more accurate sleep function for Windows. This is too slow.
  ::Sleep((BLOCK_IO_TIME_MICROS + 999) / 1000);
#else
  ::usleep(BLOCK_IO_TIME_MICROS);
#endif
}

int sim_blkread(char* ptr, unsigned block_no, void* custom) {
  sleep_to_simulate_slow_block_access();

  const int fd = *reinterpret_cast<int*>(custom);
#if defined(_WIN32)
  if (::_lseek(fd, MFAT_BLOCK_SIZE * (size_t)block_no, SEEK_SET) == -1) {
    return -1;
  }
  const size_t num_bytes = ::_read(fd, ptr, MFAT_BLOCK_SIZE);
#else
  if (::lseek(fd, MFAT_BLOCK_SIZE * (size_t)block_no, SEEK_SET) == -1) {
    return -1;
  }
  const size_t num_bytes = ::read(fd, ptr, MFAT_BLOCK_SIZE);
#endif
  return (num_bytes != MFAT_BLOCK_SIZE) && (num_bytes != 0) ? -1 : 0;
}

int sim_blkwrite(const char* ptr, unsigned block_no, void* custom) {
  sleep_to_simulate_slow_block_access();

  const int fd = *reinterpret_cast<int*>(custom);
#if defined(_WIN32)
  if (::_lseek(fd, MFAT_BLOCK_SIZE * (size_t)block_no, SEEK_SET) == -1) {
    return -1;
  }
  const size_t num_bytes = ::_write(fd, ptr, MFAT_BLOCK_SIZE);
#else
  if (::lseek(fd, MFAT_BLOCK_SIZE * (size_t)block_no, SEEK_SET) == -1) {
    return -1;
  }
  const size_t num_bytes = ::write(fd, ptr, MFAT_BLOCK_SIZE);
#endif
  return (num_bytes != MFAT_BLOCK_SIZE) && (num_bytes != 0) ? -1 : 0;
}

void mfat_stat_to_posix(const mfat_stat_t& stat, stat_t* posix_stat) {
  *posix_stat = {};

  // Size in bytes.
  posix_stat->st_size = stat.st_size;

  // File mode.
#if defined(_WIN32)
  unsigned short mode = 0;
#else
  mode_t mode = 0;
#endif
  if (stat.st_mode & MFAT_S_IFREG) {
#if defined(_WIN32)
    mode |= _S_IFREG;
#else
    mode |= S_IFREG;
#endif
  }
  if (stat.st_mode & MFAT_S_IFDIR) {
#if defined(_WIN32)
    mode |= _S_IFDIR;
#else
    mode |= S_IFDIR;
#endif
  }
  if (stat.st_mode & MFAT_S_IRUSR) {
#if defined(_WIN32)
    mode |= _S_IREAD;
#else
    mode |= S_IRUSR | S_IRGRP | S_IROTH;
#endif
  }
  if (stat.st_mode & MFAT_S_IWUSR) {
#if defined(_WIN32)
    mode |= _S_IWRITE;
#else
    mode |= S_IWUSR | S_IWGRP | S_IWOTH;
#endif
  }
  if (stat.st_mode & MFAT_S_IXUSR) {
#if defined(_WIN32)
    mode |= _S_IEXEC;
#else
    mode |= S_IXUSR | S_IXGRP | S_IXOTH;
#endif
  }
  posix_stat->st_mode = mode;

  // File time.
  struct tm tms = {};
  tms.tm_year = static_cast<int>(stat.st_mtim.year) - 1900;
  tms.tm_mon = static_cast<int>(stat.st_mtim.month) - 1;
  tms.tm_mday = static_cast<int>(stat.st_mtim.day);
  tms.tm_hour = static_cast<int>(stat.st_mtim.hour);
  tms.tm_min = static_cast<int>(stat.st_mtim.minute);
  tms.tm_sec = static_cast<int>(stat.st_mtim.second);
  posix_stat->st_mtime = mktime(&tms);
  posix_stat->st_atime = posix_stat->st_mtime;
  posix_stat->st_ctime = posix_stat->st_mtime;
}

int posix_whence_to_mfat(const int whence) {
  switch (whence) {
    case SEEK_SET:
      return MFAT_SEEK_SET;
    case SEEK_CUR:
      return MFAT_SEEK_CUR;
    case SEEK_END:
      return MFAT_SEEK_END;
    default:
      return -1;
  }
}

int host_open_flags_to_mfat(const int flags) {
  int result = 0;

#if defined(_WIN32)
  if ((flags & _O_ACCMODE) == _O_RDONLY) {
    result |= MFAT_O_RDONLY;
  } else if ((flags & _O_ACCMODE) == _O_WRONLY) {
    result |= MFAT_O_WRONLY;
  } else if ((flags & _O_ACCMODE) == _O_RDWR) {
    result |= MFAT_O_RDWR;
  }

  if (flags & _O_APPEND) {
    result |= MFAT_O_APPEND;
  }
  if (flags & _O_CREAT) {
    result |= MFAT_O_CREAT;
  }
#else
  if ((flags & O_ACCMODE) == O_RDONLY) {
    result |= MFAT_O_RDONLY;
  } else if ((flags & O_ACCMODE) == O_WRONLY) {
    result |= MFAT_O_WRONLY;
  } else if ((flags & O_ACCMODE) == O_RDWR) {
    result |= MFAT_O_RDWR;
  }

  if (flags & O_APPEND) {
    result |= MFAT_O_APPEND;
  }
  if (flags & O_CREAT) {
    result |= MFAT_O_CREAT;
  }
  if (flags & O_DIRECTORY) {
    result |= MFAT_O_DIRECTORY;
  }
#endif

  return result;
}

int mfat_fd_to_host(int fd) {
  return fd >= 0 ? (fd + 3) : fd;
}

int host_fd_to_mfat(int fd) {
  if (fd >= 0 && fd <= 2) {
    throw std::runtime_error("Invalid mfat file descriptor.");
  }
  return fd >= 3 ? (fd - 3) : fd;
}
}  // namespace

syscalls_t::syscalls_t(ram_t& ram) : m_ram(ram) {
}

syscalls_t::~syscalls_t() {
  // Close open directories.
  for (int i = 0; i < MAX_OPEN_DIRS; ++i) {
    auto* dirp = m_open_dirs[i];
    if (dirp != nullptr) {
      sim_closedir(dirp);
    }
  }

  // TODO(m): Close open fd:s etc.

  // In case a disk image is mounted, unmount it.
  unmount_disk_image();
}

bool syscalls_t::mount_disk_image(const std::string& disk_image_path) {
  // Open the disk image file.
#if defined(_WIN32)
  m_disk_fd = ::_open(disk_image_path.c_str(), _O_BINARY | _O_RDWR);
#else
  m_disk_fd = ::open(disk_image_path.c_str(), O_RDWR);
#endif
  if (m_disk_fd == -1) {
    return false;
  }

  // Mount the disk image in mfat, using custom block I/O functions.
  if (mfat_mount(sim_blkread, sim_blkwrite, &m_disk_fd) == -1) {
    unmount_disk_image();
    return false;
  }

  return true;
}

void syscalls_t::unmount_disk_image() {
  // Close the disk image file.
  if (m_disk_fd != -1) {
#if defined(_WIN32)
    ::_close(m_disk_fd);
#else
    ::close(m_disk_fd);
#endif
    m_disk_fd = -1;
  }
}

void syscalls_t::clear() {
  m_terminate = false;
  m_exit_code = 0u;
}

void syscalls_t::call(const uint32_t routine_no, std::array<uint32_t, 33>& regs) {
  if (routine_no >= static_cast<uint32_t>(routine_t::LAST_)) {
    throw std::runtime_error("Invalid simulator syscall.");
  }
  const auto routine = static_cast<routine_t>(routine_no);
  switch (routine) {
    case routine_t::EXIT:
      sim_exit(static_cast<int>(regs[1]));
      break;

    case routine_t::PUTCHAR:
      regs[1] = static_cast<uint32_t>(sim_putchar(static_cast<int>(regs[1])));
      break;

    case routine_t::GETCHAR:
      regs[1] = static_cast<uint32_t>(sim_getchar());
      break;

    case routine_t::CLOSE:
      regs[1] = static_cast<uint32_t>(sim_close(fd_to_host(regs[1])));
      break;

    case routine_t::FSTAT: {
      stat_t buf;
      regs[1] = static_cast<uint32_t>(sim_fstat(fd_to_host(regs[1]), &buf));
      stat_to_ram(buf, regs[2]);
    } break;

    case routine_t::ISATTY:
      regs[1] = static_cast<uint32_t>(sim_isatty(fd_to_host(regs[1])));
      break;

    case routine_t::LINK:
      regs[1] = static_cast<uint32_t>(
          sim_link(path_to_host(regs[1]).c_str(), path_to_host(regs[2]).c_str()));
      break;

    case routine_t::LSEEK:
      regs[1] = static_cast<uint32_t>(
          sim_lseek(fd_to_host(regs[1]), static_cast<int>(regs[2]), static_cast<int>(regs[3])));
      break;

    case routine_t::MKDIR:
      regs[1] = static_cast<uint32_t>(
          sim_mkdir(path_to_host(regs[1]).c_str(), static_cast<int>(regs[2])));
      break;

    case routine_t::OPEN:
      regs[1] = fd_to_guest(sim_open(
          path_to_host(regs[1]).c_str(), open_flags_to_host(regs[2]), open_mode_to_host(regs[3])));
      break;

    case routine_t::READ: {
      if (!m_ram.valid_range(regs[2], regs[3])) {
        regs[1] = static_cast<uint32_t>(-1);
      }
      int fd = fd_to_host(regs[1]);
      char* buf = reinterpret_cast<char*>(&m_ram.at(regs[2]));
      int nbytes = static_cast<int>(regs[3]);
      regs[1] = static_cast<uint32_t>(sim_read(fd, buf, nbytes));
    } break;

    case routine_t::STAT: {
      stat_t buf;
      regs[1] = static_cast<uint32_t>(sim_stat(path_to_host(regs[1]).c_str(), &buf));
      stat_to_ram(buf, regs[2]);
    } break;

    case routine_t::UNLINK:
      regs[1] = static_cast<uint32_t>(sim_unlink(path_to_host(regs[1]).c_str()));
      break;

    case routine_t::WRITE: {
      if (!m_ram.valid_range(regs[2], regs[3])) {
        regs[1] = static_cast<uint32_t>(-1);
      }
      int fd = fd_to_host(regs[1]);
      const char* buf = reinterpret_cast<const char*>(&m_ram.at(regs[2]));
      int nbytes = static_cast<int>(regs[3]);
      regs[1] = static_cast<uint32_t>(sim_write(fd, buf, nbytes));
    } break;

    case routine_t::GETTIMEMICROS: {
      const auto result = sim_gettimemicros();
      regs[1] = static_cast<uint32_t>(result);
      regs[2] = static_cast<uint32_t>(result >> 32);
    } break;

    case routine_t::RMDIR:
      regs[1] = static_cast<uint32_t>(sim_rmdir(path_to_host(regs[1]).c_str()));
      break;

    case routine_t::OPENDIR:
      regs[1] = dirp_to_guest(sim_opendir(path_to_host(regs[1]).c_str()));
      break;

    case routine_t::FDOPENDIR:
      regs[1] = dirp_to_guest(sim_fdopendir(fd_to_host(regs[1])));
      break;

    case routine_t::CLOSEDIR:
      regs[1] = sim_closedir(dirp_to_host(regs[1]));
      break;

    case routine_t::READDIR: {
      dirent_t ent = sim_readdir(dirp_to_host(regs[1]));
      if (ent != nullptr) {
        // readdir() clobbers previous results (it's not reentrant).
        regs[1] = DIRENT_ADDR;
        dirent_to_ram(ent, DIRENT_ADDR);
      } else {
        regs[1] = 0U;
      }
    } break;

    case routine_t::GETARGUMENTS: {
      uint32_t argc = m_ram.load32(SIM_ARGS_START);
      uint32_t argv = SIM_ARGS_START + 4;
      m_ram.store32(regs[1], argc);
      m_ram.store32(regs[2], argv);
    } break;

    default:
      throw std::runtime_error("Invalid simulator syscall.");
      break;
  }
}

void syscalls_t::stat_to_ram(stat_t& buf, uint32_t addr) {
  // MRISC32 type (from newlib):
  //    struct stat
  //    {
  //      dev_t            st_dev;        // 0  (uint16_t)
  //      ino_t            st_ino;        // 2  (uint16_t)
  //      mode_t           st_mode;       // 4  (uint32_t)
  //      nlink_t          st_nlink;      // 8  (uint16_t)
  //      uid_t            st_uid;        // 10 (uint16_t)
  //      gid_t            st_gid;        // 12 (uint16_t)
  //      dev_t            st_rdev;       // 14 (uint16_t)
  //      off_t            st_size;       // 16 (uint32_t)
  //      struct timespec  st_atim;       // 20 (uint64_t + uint32_t)
  //      struct timespec  st_mtim;       // 32 (uint64_t + uint32_t)
  //      struct timespec  st_ctim;       // 44 (uint64_t + uint32_t)
  //      blksize_t        st_blksize;    // 56 (uint32_t)
  //      blkcnt_t         st_blocks;     // 60 (uint32_t)
  //      long             st_spare4[2];  // 64 (uint32_t * 2)
  //    };                                // Total size: 72
  m_ram.store16(addr + 0, buf.st_dev);
  m_ram.store16(addr + 2, buf.st_ino);
  m_ram.store32(addr + 4, buf.st_mode);
  m_ram.store16(addr + 8, buf.st_nlink);
  m_ram.store16(addr + 10, buf.st_uid);
  m_ram.store16(addr + 12, buf.st_gid);
  m_ram.store16(addr + 14, buf.st_rdev);
  m_ram.store32(addr + 16, buf.st_size);
#if defined(_WIN32)
  m_ram.store32(addr + 20, static_cast<uint32_t>(buf.st_atime));
  m_ram.store32(addr + 24, static_cast<uint32_t>(buf.st_atime >> 32));
  m_ram.store32(addr + 28, 0U);
  m_ram.store32(addr + 32, static_cast<uint32_t>(buf.st_mtime));
  m_ram.store32(addr + 36, static_cast<uint32_t>(buf.st_mtime >> 32));
  m_ram.store32(addr + 40, 0U);
  m_ram.store32(addr + 44, static_cast<uint32_t>(buf.st_ctime));
  m_ram.store32(addr + 48, static_cast<uint32_t>(buf.st_ctime >> 32));
  m_ram.store32(addr + 52, 0U);
#elif defined(__APPLE__)
  m_ram.store32(addr + 20, static_cast<uint32_t>(buf.st_atimespec.tv_sec));
  m_ram.store32(addr + 24, static_cast<uint32_t>(buf.st_atimespec.tv_sec >> 32));
  m_ram.store32(addr + 28, static_cast<uint32_t>(buf.st_atimespec.tv_nsec));
  m_ram.store32(addr + 32, static_cast<uint32_t>(buf.st_mtimespec.tv_sec));
  m_ram.store32(addr + 36, static_cast<uint32_t>(buf.st_mtimespec.tv_sec >> 32));
  m_ram.store32(addr + 40, static_cast<uint32_t>(buf.st_mtimespec.tv_nsec));
  m_ram.store32(addr + 44, static_cast<uint32_t>(buf.st_ctimespec.tv_sec));
  m_ram.store32(addr + 48, static_cast<uint32_t>(buf.st_ctimespec.tv_sec >> 32));
  m_ram.store32(addr + 52, static_cast<uint32_t>(buf.st_ctimespec.tv_nsec));
#else
  m_ram.store32(addr + 20, static_cast<uint32_t>(buf.st_atim.tv_sec));
  m_ram.store32(addr + 24, static_cast<uint32_t>(buf.st_atim.tv_sec >> 32));
  m_ram.store32(addr + 28, buf.st_atim.tv_nsec);
  m_ram.store32(addr + 32, static_cast<uint32_t>(buf.st_mtim.tv_sec));
  m_ram.store32(addr + 36, static_cast<uint32_t>(buf.st_mtim.tv_sec >> 32));
  m_ram.store32(addr + 40, buf.st_mtim.tv_nsec);
  m_ram.store32(addr + 44, static_cast<uint32_t>(buf.st_ctim.tv_sec));
  m_ram.store32(addr + 48, static_cast<uint32_t>(buf.st_ctim.tv_sec >> 32));
  m_ram.store32(addr + 52, buf.st_ctim.tv_nsec);
#endif
#if defined(_WIN32)
  const uint32_t blksize = 512U;
  const uint32_t blocks = static_cast<uint32_t>(buf.st_size + (blksize - 1U)) / blksize;
  m_ram.store32(addr + 56, blksize);
  m_ram.store32(addr + 60, blocks);
#else
  m_ram.store32(addr + 56, buf.st_blksize);
  m_ram.store32(addr + 60, buf.st_blocks);
#endif
}

std::string syscalls_t::path_to_host(uint32_t addr) {
  std::string result;
  while (true) {
    const auto c = m_ram.load8(addr++);
    if (c == 0u) {
      break;
    }
    result += static_cast<char>(c);
  }
  // TODO(m): Map the path to a suitable host file system path.
  return result;
}

int syscalls_t::fd_to_host(uint32_t fd) {
  // TODO(m): Use a translation map.
  return static_cast<int>(fd);
}

uint32_t syscalls_t::fd_to_guest(int fd) {
  // TODO(m): Use a translation map.
  return static_cast<uint32_t>(fd);
}

int syscalls_t::open_flags_to_host(uint32_t flags) {
  int result;

#if defined(_WIN32)
  if ((flags & 0x0003u) == 1)
    result = _O_WRONLY;
  else if ((flags & 0x0003u) == 2)
    result = _O_RDWR;
  else
    result = _O_RDONLY;

  if ((flags & 0x0008u) != 0u)
    result |= _O_APPEND;
  if ((flags & 0x0200u) != 0u)
    result |= _O_CREAT;
  if ((flags & 0x0400u) != 0u)
    result |= _O_TRUNC;

  // Always open in binary mode (as other POSIX systems).
  result |= _O_BINARY;
#else
  if ((flags & 0x0003u) == 1)
    result = O_WRONLY;
  else if ((flags & 0x0003u) == 2)
    result = O_RDWR;
  else
    result = O_RDONLY;

  if ((flags & 0x0008u) != 0u)
    result |= O_APPEND;
  if ((flags & 0x0200u) != 0u)
    result |= O_CREAT;
  if ((flags & 0x0400u) != 0u)
    result |= O_TRUNC;
#endif

  return result;
}

int syscalls_t::open_mode_to_host(uint32_t mode) {
  int result;

#if defined(_WIN32)
  result = 0;
  if ((mode & 00400) != 0U)  // S_IRUSR - user read
    result |= _S_IREAD;
  if ((mode & 00200) != 0U)  // S_IWUSR - user write
    result |= _S_IWRITE;
#else
  result = mode;
#endif

  return result;
}

dir_handle_t syscalls_t::dirp_to_host(uint32_t dirp) {
  if (dirp == 0U) {
    return nullptr;
  }
  auto idx = static_cast<int>((dirp - DIRP_BASE_ADDR) / 4U);
  if (idx < 0 || idx >= MAX_OPEN_DIRS) {
    return nullptr;
  }
  return m_open_dirs[idx];
}

uint32_t syscalls_t::dirp_to_guest(dir_handle_t dirp) {
  if (dirp == nullptr) {
    return 0U;
  }
  for (int i = 0; i < MAX_OPEN_DIRS; ++i) {
    if (m_open_dirs[i] == nullptr) {
      m_open_dirs[i] = dirp;
      return DIRP_BASE_ADDR + static_cast<uint32_t>(4 * i);
    }
  }
  sim_closedir(dirp);
  return 0U;
}

void syscalls_t::dirent_to_ram(dirent_t ent, uint32_t addr) {
  // MRISC32 type (from newlib):
  //    struct dirent {
  //      ino_t  d_ino;                   // 0 (uint16_t)
  //      char   d_name[255 + 1];         // 2 (uint8_t * 256)
  //    };
#if defined(_WIN32)
  auto* dirent = reinterpret_cast<win_dirent_t*>(ent);
  uint32_t d_ino = 0U;
  const char* d_name = dirent->find_file_data.cFileName;
#else
  auto* dirent = reinterpret_cast<struct dirent*>(ent);
  uint32_t d_ino = static_cast<uint32_t>(dirent->d_ino);
  const char* d_name = dirent->d_name;
#endif
  m_ram.store16(addr + 0, d_ino);

  // Copy d_name as a zero-terminated string.
  auto name_addr = addr + 2;
  static constexpr int MAX_NAME_LEN = 255;
  for (int i = 0; i < MAX_NAME_LEN; ++i) {
    auto c = d_name[i];
    if (c == 0) {
      break;
    }
    m_ram.store8(name_addr, c);
    ++name_addr;
  }
  m_ram.store8(name_addr, 0);
}

void syscalls_t::sim_exit(int status) {
  m_terminate = true;
  m_exit_code = static_cast<uint32_t>(status);
}

int syscalls_t::sim_putchar(int c) {
  return ::putchar(c);
}

int syscalls_t::sim_getchar(void) {
  return ::getchar();
}

int syscalls_t::sim_close(int fd) {
  if (fd >= 0 && fd <= 2) {
    // We don't want to close stdin (0), stdout (1) or stderr (2), since they are used by the
    // simulator.
    return 0;
  }
  if (m_disk_fd != -1) {
    return mfat_close(host_fd_to_mfat(fd));
  }
#if defined(_WIN32)
  return ::_close(fd);
#else
  return ::close(fd);
#endif
}

int syscalls_t::sim_fstat(int fd, stat_t* buf) {
  if (m_disk_fd != -1 && (fd < 0 || fd > 2)) {
    mfat_stat_t s;
    int result = mfat_fstat(host_fd_to_mfat(fd), &s);
    mfat_stat_to_posix(s, buf);
    return result;
  }
#if defined(_WIN32)
  return ::_fstat64(fd, buf);
#else
  return ::fstat(fd, buf);
#endif
}

int syscalls_t::sim_isatty(int fd) {
  if (m_disk_fd != -1 && (fd < 0 || fd > 2)) {
    return 0;
  }
#if defined(_WIN32)
  return ::_isatty(fd);
#else
  return ::isatty(fd);
#endif
}

int syscalls_t::sim_link(const char* oldpath, const char* newpath) {
  if (m_disk_fd != -1) {
    // TODO(m): Implement me!
    // return mfat_link(oldpath, newpath);
    (void)oldpath;
    (void)newpath;
    return -1;
  }
#if defined(_WIN32)
  const auto success = (CreateHardLinkA(newpath, oldpath, nullptr) != 0);
  return success ? 0 : -1;
#else
  return ::link(oldpath, newpath);
#endif
}

int syscalls_t::sim_lseek(int fd, int offset, int whence) {
  if (m_disk_fd != -1 && (fd < 0 || fd > 2)) {
    return mfat_lseek(host_fd_to_mfat(fd), offset, posix_whence_to_mfat(whence));
  }
#if defined(_WIN32)
  return ::_lseek(fd, offset, whence);
#else
  return ::lseek(fd, offset, whence);
#endif
}

int syscalls_t::sim_mkdir(const char* pathname, int mode) {
  if (m_disk_fd != -1) {
    // TODO(m): Implement me!
    // return mfat_mkdir(pathname, mode);
    (void)pathname;
    (void)mode;
    return -1;
  }
#if defined(_WIN32)
  (void)mode;
  return ::_mkdir(pathname);
#else
  return ::mkdir(pathname, static_cast<mode_t>(mode));
#endif
}

int syscalls_t::sim_open(const char* pathname, int flags, int mode) {
  if (m_disk_fd != -1) {
    (void)mode;
    return mfat_fd_to_host(mfat_open(pathname, host_open_flags_to_mfat(flags)));
  }
#if defined(_WIN32)
  return ::_open(pathname, flags, mode);
#else
  return ::open(pathname, flags, mode);
#endif
}

int syscalls_t::sim_read(int fd, char* buf, int nbytes) {
  if (m_disk_fd != -1 && (fd < 0 || fd > 2)) {
    if (nbytes < 0) {
      return -1;
    }
    return mfat_read(host_fd_to_mfat(fd), buf, static_cast<uint32_t>(nbytes));
  }
#if defined(_WIN32)
  return ::_read(fd, buf, nbytes);
#else
  return ::read(fd, buf, nbytes);
#endif
}

int syscalls_t::sim_stat(const char* path, stat_t* buf) {
  if (m_disk_fd != -1) {
    mfat_stat_t s;
    int result = mfat_stat(path, &s);
    mfat_stat_to_posix(s, buf);
    return result;
  }
#if defined(_WIN32)
  return ::_stat64(path, buf);
#else
  return ::stat(path, buf);
#endif
}

int syscalls_t::sim_unlink(const char* pathname) {
  if (m_disk_fd != -1) {
    // TODO(m): Implement me!
    // return mfat_unlink(pathname);
    (void)pathname;
    return -1;
  }
#if defined(_WIN32)
  return ::_unlink(pathname);
#else
  return ::unlink(pathname);
#endif
}

int syscalls_t::sim_write(int fd, const char* buf, int nbytes) {
  if (m_disk_fd != -1 && (fd < 0 || fd > 2)) {
    if (nbytes < 0) {
      return -1;
    }
    return mfat_write(host_fd_to_mfat(fd), buf, static_cast<uint32_t>(nbytes));
  }
#if defined(_WIN32)
  return ::_write(fd, buf, nbytes);
#else
  return ::write(fd, buf, nbytes);
#endif
}

unsigned long long syscalls_t::sim_gettimemicros(void) {
#if defined(_WIN32)
  static double s_micros_per_count;
  static bool s_got_freq = false;
  if (!s_got_freq) {
    LARGE_INTEGER freq;
    QueryPerformanceFrequency(&freq);
    s_micros_per_count = 1000000.0 / static_cast<double>(freq.QuadPart);
    s_got_freq = true;
  }

  LARGE_INTEGER count;
  QueryPerformanceCounter(&count);
  auto micros = s_micros_per_count * static_cast<double>(count.QuadPart);
  return static_cast<unsigned long long>(micros);
#else
  struct timeval tv;
  if (::gettimeofday(&tv, nullptr) == 0) {
    return static_cast<unsigned long long>(tv.tv_sec) * 1000000ULL +
           static_cast<unsigned long long>(tv.tv_usec);
  } else {
    return 0;
  }
#endif
}

int syscalls_t::sim_rmdir(const char* pathname) {
  if (m_disk_fd != -1) {
    // TODO(m): Implement me!
    // return mfat_rmdir(pathname);
    (void)pathname;
    return -1;
  }
#if defined(_WIN32)
  return ::_rmdir(pathname);
#else
  return ::rmdir(pathname);
#endif
}

dir_handle_t syscalls_t::sim_opendir(const char* dirname) {
  if (m_disk_fd != -1) {
    return reinterpret_cast<dir_handle_t>(mfat_opendir(dirname));
  }
#if defined(_WIN32)
  // Convert the dirname to a glob pattern suitable for FindFirstFile.
  char glob[MAX_PATH];
  constexpr int max_dirname_len = MAX_PATH - 3;
  int len = -1;
  for (int i = 0; i < max_dirname_len; ++i) {
    char c = dirname[i];
    if (c == '/') {
      c = '\\';
    } else if (c == 0) {
      len = i;
      break;
    }
    glob[i] = c;
  }
  while (len > 0 && glob[len - 1] == '\\') {
    --len;
  }
  if (len <= 0) {
    return nullptr;
  }
  glob[len] = '\\';
  glob[len + 1] = '*';
  glob[len + 2] = 0;

  // Allocate a custom "DIR" object, and call FindFirstFile.
  auto* dir = new win_dir_t;
  if (dir == nullptr) {
    return nullptr;
  }
  dir->handle = FindFirstFileA(glob, &dir->dirent.find_file_data);
  if (dir->handle == INVALID_HANDLE_VALUE) {
    delete dir;
    return nullptr;
  }

  // Tell sim_readdir() that this is the first dir item, which is already present in find_file_data.
  dir->first = true;
  return reinterpret_cast<dir_handle_t>(dir);
#else
  return reinterpret_cast<dir_handle_t>(::opendir(dirname));
#endif
}

dir_handle_t syscalls_t::sim_fdopendir(int fd) {
  if (m_disk_fd != -1) {
    return reinterpret_cast<dir_handle_t>(mfat_fdopendir(host_fd_to_mfat(fd)));
  }
#if defined(_WIN32)
  // TODO(m): Implement me!
  return nullptr;
#else
  return reinterpret_cast<dir_handle_t>(::fdopendir(fd));
#endif
}

int syscalls_t::sim_closedir(dir_handle_t dirp) {
  if (dirp == nullptr) {
    return -1;
  }
  if (m_disk_fd != -1) {
    return mfat_closedir(reinterpret_cast<mfat_dir_t*>(dirp));
  }
#if defined(_WIN32)
  auto* dir = reinterpret_cast<win_dir_t*>(dirp);
  FindClose(dir->handle);
  delete dir;
  return 0;
#else
  return ::closedir(reinterpret_cast<DIR*>(dirp));
#endif
}

dirent_t syscalls_t::sim_readdir(dir_handle_t dirp) {
  if (dirp == nullptr) {
    return nullptr;
  }
  if (m_disk_fd != -1) {
    mfat_dirent_t* ent = mfat_readdir(reinterpret_cast<mfat_dir_t*>(dirp));
    if (ent == nullptr) {
      return nullptr;
    }

#if defined(_WIN32)
    // TODO(m): Implement me!
    return nullptr;
#else
    // Convert the MFAT dirent to a POSIX dirent.
    // Note: It's OK to use a static variable here, as the result is immediately copied to RAM.
    static struct dirent s_dirent;
    s_dirent = {};
    std::strncpy(s_dirent.d_name, ent->d_name, sizeof(s_dirent.d_name) - 1);
    s_dirent.d_name[sizeof(s_dirent.d_name) - 1] = 0;
    return &s_dirent;
#endif
  }
#if defined(_WIN32)
  auto* dir = reinterpret_cast<win_dir_t*>(dirp);
  if (dir->first) {
    dir->first = false;
    return reinterpret_cast<dirent_t>(&dir->dirent);
  }
  if (FindNextFileA(dir->handle, &dir->dirent.find_file_data)) {
    return reinterpret_cast<dirent_t>(&dir->dirent);
  }
  return nullptr;
#else
  return reinterpret_cast<dirent_t>(::readdir(reinterpret_cast<DIR*>(dirp)));
#endif
}
